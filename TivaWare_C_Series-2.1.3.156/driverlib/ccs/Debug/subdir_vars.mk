################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
S_SRCS += \
D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/epi_workaround_ccs.s 

C_SRCS += \
D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/adc.c \
D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/aes.c \
D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/can.c \
D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/comp.c \
D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/cpu.c \
D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/crc.c \
D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/des.c \
D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/eeprom.c \
D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/emac.c \
D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/epi.c \
D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/flash.c \
D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/fpu.c \
D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/gpio.c \
D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/hibernate.c \
D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/i2c.c \
D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/interrupt.c \
D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/lcd.c \
D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/mpu.c \
D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/onewire.c \
D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/pwm.c \
D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/qei.c \
D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/shamd5.c \
D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/ssi.c \
D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/sw_crc.c \
D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/sysctl.c \
D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/sysexc.c \
D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/systick.c \
D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/timer.c \
D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/uart.c \
D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/udma.c \
D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/usb.c \
D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/watchdog.c 

S_DEPS += \
./epi_workaround_ccs.d 

C_DEPS += \
./adc.d \
./aes.d \
./can.d \
./comp.d \
./cpu.d \
./crc.d \
./des.d \
./eeprom.d \
./emac.d \
./epi.d \
./flash.d \
./fpu.d \
./gpio.d \
./hibernate.d \
./i2c.d \
./interrupt.d \
./lcd.d \
./mpu.d \
./onewire.d \
./pwm.d \
./qei.d \
./shamd5.d \
./ssi.d \
./sw_crc.d \
./sysctl.d \
./sysexc.d \
./systick.d \
./timer.d \
./uart.d \
./udma.d \
./usb.d \
./watchdog.d 

OBJS += \
./adc.obj \
./aes.obj \
./can.obj \
./comp.obj \
./cpu.obj \
./crc.obj \
./des.obj \
./eeprom.obj \
./emac.obj \
./epi.obj \
./epi_workaround_ccs.obj \
./flash.obj \
./fpu.obj \
./gpio.obj \
./hibernate.obj \
./i2c.obj \
./interrupt.obj \
./lcd.obj \
./mpu.obj \
./onewire.obj \
./pwm.obj \
./qei.obj \
./shamd5.obj \
./ssi.obj \
./sw_crc.obj \
./sysctl.obj \
./sysexc.obj \
./systick.obj \
./timer.obj \
./uart.obj \
./udma.obj \
./usb.obj \
./watchdog.obj 

OBJS__QUOTED += \
"adc.obj" \
"aes.obj" \
"can.obj" \
"comp.obj" \
"cpu.obj" \
"crc.obj" \
"des.obj" \
"eeprom.obj" \
"emac.obj" \
"epi.obj" \
"epi_workaround_ccs.obj" \
"flash.obj" \
"fpu.obj" \
"gpio.obj" \
"hibernate.obj" \
"i2c.obj" \
"interrupt.obj" \
"lcd.obj" \
"mpu.obj" \
"onewire.obj" \
"pwm.obj" \
"qei.obj" \
"shamd5.obj" \
"ssi.obj" \
"sw_crc.obj" \
"sysctl.obj" \
"sysexc.obj" \
"systick.obj" \
"timer.obj" \
"uart.obj" \
"udma.obj" \
"usb.obj" \
"watchdog.obj" 

C_DEPS__QUOTED += \
"adc.d" \
"aes.d" \
"can.d" \
"comp.d" \
"cpu.d" \
"crc.d" \
"des.d" \
"eeprom.d" \
"emac.d" \
"epi.d" \
"flash.d" \
"fpu.d" \
"gpio.d" \
"hibernate.d" \
"i2c.d" \
"interrupt.d" \
"lcd.d" \
"mpu.d" \
"onewire.d" \
"pwm.d" \
"qei.d" \
"shamd5.d" \
"ssi.d" \
"sw_crc.d" \
"sysctl.d" \
"sysexc.d" \
"systick.d" \
"timer.d" \
"uart.d" \
"udma.d" \
"usb.d" \
"watchdog.d" 

S_DEPS__QUOTED += \
"epi_workaround_ccs.d" 

C_SRCS__QUOTED += \
"D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/adc.c" \
"D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/aes.c" \
"D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/can.c" \
"D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/comp.c" \
"D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/cpu.c" \
"D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/crc.c" \
"D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/des.c" \
"D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/eeprom.c" \
"D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/emac.c" \
"D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/epi.c" \
"D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/flash.c" \
"D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/fpu.c" \
"D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/gpio.c" \
"D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/hibernate.c" \
"D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/i2c.c" \
"D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/interrupt.c" \
"D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/lcd.c" \
"D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/mpu.c" \
"D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/onewire.c" \
"D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/pwm.c" \
"D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/qei.c" \
"D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/shamd5.c" \
"D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/ssi.c" \
"D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/sw_crc.c" \
"D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/sysctl.c" \
"D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/sysexc.c" \
"D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/systick.c" \
"D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/timer.c" \
"D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/uart.c" \
"D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/udma.c" \
"D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/usb.c" \
"D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/watchdog.c" 

S_SRCS__QUOTED += \
"D:/ti/TivaWare_C_Series-2.1.3.156/driverlib/epi_workaround_ccs.s" 


